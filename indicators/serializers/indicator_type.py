from rest_framework import serializers
from indicators.models.indicator_type import IndicatorType


class IndicatorTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = IndicatorType
        fields = (
            'id',
            'name',
            'created_at',
            'updated_at'
        )
        read_only_fields = (
            'id',
        )