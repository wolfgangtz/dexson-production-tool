from rest_framework import serializers
from indicators.models.indicator import Indicator


class IndicatorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Indicator
        fields = (
            'id',
            'is_text',
            'is_date',
            'target',
            'indicator_type',
            'value',
            'target_value_spot',
            'target_value_ytd',
            'created_at',
            'updated_at'
        )
        read_only_fields = (
            'id',
        )