from django.db import models
from indicators.models.indicator_type import IndicatorType

class Indicator(models.Model):

    is_text = models.BooleanField(
        default=False
    )

    is_date = models.BooleanField(
        default=False
    )

    target = models.BooleanField(
        default=False
    )

    indicator_type = models.ForeignKey(
        IndicatorType,
        on_delete=models.PROTECT,
        null=False,
        blank=False
    )

    value = models.CharField(
        max_length=30
    )

    target_value_spot = models.CharField(
        max_length=50,
        null=True,
        blank=True
    )

    target_value_ytd = models.CharField(
        max_length=50,
        null=True,
        blank=True
    )

    created_at = models.DateField(
        auto_now_add=True
    )

    updated_at = models.DateField(
        auto_now_add=True
    )