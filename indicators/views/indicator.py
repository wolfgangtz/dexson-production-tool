from rest_framework import generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from indicators.models.indicator import Indicator
from indicators.serializers.indicator import IndicatorSerializer

@permission_classes((IsAuthenticated, ))
class IndicatorList(generics.ListCreateAPIView):
    queryset = Indicator.objects.all()
    serializer_class = IndicatorSerializer
    name = 'indicator-list'

@permission_classes((IsAuthenticated, ))
class IndicatorDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Indicator.objects.all()
    serializer_class = IndicatorSerializer
    name = 'indicator-detail'

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)