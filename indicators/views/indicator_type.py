from rest_framework import generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from indicators.models.indicator_type import IndicatorType
from indicators.serializers.indicator_type import IndicatorTypeSerializer

@permission_classes((IsAuthenticated, ))
class IndicatorTypeList(generics.ListCreateAPIView):
    queryset = IndicatorType.objects.all()
    serializer_class = IndicatorTypeSerializer
    name = 'indicator-type-list'

@permission_classes((IsAuthenticated, ))
class IndicatorTypeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = IndicatorType.objects.all()
    serializer_class = IndicatorTypeSerializer
    name = 'indicator-type-detail'

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)