from django.urls import path
from django.urls import re_path
from indicators.views import indicator
from indicators.views import indicator_type

urlpatterns = [
    path(
        '',
        indicator.IndicatorList.as_view(),
        name=indicator.IndicatorList.name
    ),
    re_path(
        '^(?P<pk>[0-9]+)/$',
        indicator.IndicatorDetail.as_view(),
        name=indicator.IndicatorDetail.name
    ),
    path(
        'type/',
        indicator_type.IndicatorTypeList.as_view(),
        name=indicator_type.IndicatorTypeList.name
    ),
    re_path(
        '^type/(?P<pk>[0-9]+)/$',
        indicator_type.IndicatorTypeDetail.as_view(),
        name=indicator_type.IndicatorTypeDetail.name
    )
]