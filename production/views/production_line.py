from rest_framework import generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from production.models.production_line import ProductionLine
from production.serializers.production_line import ProductionLineSerializer

@permission_classes((IsAuthenticated, ))
class ProductionLineList(generics.ListCreateAPIView):
    queryset = ProductionLine.objects.all()
    serializer_class = ProductionLineSerializer
    name = 'production-line-list'

@permission_classes((IsAuthenticated, ))
class ProductionLineDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ProductionLine.objects.all()
    serializer_class = ProductionLineSerializer
    name = 'production-line-detail'

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)