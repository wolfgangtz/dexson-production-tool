from rest_framework import generics
from django.shortcuts import render
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from production.models.area import Area
from production.models.production_line import ProductionLine
from production.serializers.area import AreaSerializer
from production.helpers.helpers_function import get_message_by_key


RENDER_AREAS_NAME = "render-areas"
RENDER_PRODUCTION_LINES_BY_AREA_NAME = "render-production-lines-by-area"

@permission_classes((IsAuthenticated, ))
class AreaList(generics.ListCreateAPIView):
    queryset = Area.objects.all()
    serializer_class = AreaSerializer
    name = 'area-list'

@permission_classes((IsAuthenticated, ))
class AreaDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Area.objects.all()
    serializer_class = AreaSerializer
    name = 'area-detail'

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

@permission_classes((IsAuthenticated, ))
def render_areas(request):
    areas = Area.objects.all()
    return render(
        request,
        'list_areas.html',
            {
                'areas': areas,
                'message_list_areas': get_message_by_key('MESSAGE_LIST_AREAS'),
                'area_name': get_message_by_key('MESSAGE_TITTLE_NAME_AREA')
            }
    )

@permission_classes((IsAuthenticated, ))
def render_production_lines_by_area(request, area_id):
    area = Area.objects.get(pk=area_id)
    production_lines = [production_line for production_line in ProductionLine.objects.filter(area=area) if production_line.get_op_queue != 0 ]
    return render(
        request,
        'list_production_lines.html',
            {
                'production_lines': production_lines,
                'message_list_production_lines': get_message_by_key('MESSAGE_LIST_PRODUCTION_LINES'),
                'production_line_name': get_message_by_key('MESSAGE_TITTLE_NAME_PRODUCTION_LINE'),
                'production_line_po_open': get_message_by_key('MESSAGE_PRODUCTION_LINE_PO_OPEN'),
                'queue_time': get_message_by_key('MESSAGE_PRODUCTION_LINE_QUEUE_TIME')
            }
    )