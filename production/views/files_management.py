from django import forms
import pandas as pd
from django.shortcuts import render
from django.conf import settings
from production.forms import UploadFileForm
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from production.models.production_line import ProductionLine
from product.models.dxn import DXN
from production.models.area import Area
from production.models.production_order_status import ProductionOrderStatus
from production.models.production_order import ProductionOrder
from production.serializers.production_order import ProductionOrderSerializer
from production.helpers.helpers_function import get_message_by_key
from django.http import HttpResponseRedirect


def validate_duplicity(name_machine):

    tc_50_names = ['EXTRUSORA TC 50', 'EXTRUSORA TC50', 'EXTRUSORA TC5O', 'EXTRUSORA TC 51']
    cm_35 = ['EXTRUSORA CM 35', 'EXTRUSORA CM35']
    con_40 = ['EXTRUSORA CON 40', 'EXTRUSORA CON40']
    con_50 = ['EXTRUSORA CON 50', 'EXTRUSORA CON50']
    krauss_maffei = ['EXTRUSORA KRAUSS MAFFEI', 'KRAUSS MAFEI']
    tc_65 = ['EXTRUSORA TC 65', 'EXTRUSORA TC65']
    inyect_150 = ['INYECTORA 150', 'INYECTORA150']
    inyect_420 = ['INYECTORA 420', 'INYECTORA 420A', 'MAQUINA 420']
    tc_55c = ['EXTRUSORA TC55c', 'EXTRUSORA TC55C']
    manual_kit = ['MANUAL KIT', 'MANUAL KITS']

    if name_machine in tc_50_names:
        return 'EXTRUSORA TC50'

    if name_machine in cm_35:
        return 'EXTRUSORA CM35'

    if name_machine in con_40:
        return 'EXTRUSORA CON40'

    if name_machine in con_50:
        return 'EXTRUSORA CON50'

    if name_machine in krauss_maffei:
        return 'EXTRUSORA KRAUSS MAFFEI'

    if name_machine in tc_65:
        return 'EXTRUSORA TC65'

    if name_machine in inyect_150:
        return 'INYECTORA 150'

    if name_machine in inyect_420:
        return 'INYECTORA 420'

    if name_machine in tc_55c:
        return 'EXTRUSORA TC55C'

    if name_machine in manual_kit:
        return 'MANUAL KIT'

    return name_machine

def get_area_name(area):
    if area == 'EXTRUSIO':
        area = settings.AREA_NAME_1
    elif area == 'MEZCLA':
        area = settings.AREA_NAME_3
    elif area == 'INYECCIO':
        area = settings.AREA_NAME_2
    elif area == 'TROQUELA':
        area = settings.AREA_NAME_5
    elif area == 'ENSAMBLE':
        area = settings.AREA_NAME_4
    else:
        area = settings.AREA_NAME_1

    selected_area = Area.objects.get(name=area)
    return selected_area.id

def get_status(schedule, delivered):
    status = 0
    if schedule == delivered:
        status = settings.PRODUCTION_ORDER_STATE_4
    elif delivered == 0:
        status = settings.PRODUCTION_ORDER_STATE_1
    elif delivered > 0 and delivered < schedule:
        status = settings.PRODUCTION_ORDER_STATE_3
    status_selected = ProductionOrderStatus.objects.get(name=status)
    return status_selected.id

def handle_uploaded_file(file_1, file_2):

    with open(settings.MEDIA_XMLS_FILES + '/operaciones.xlsx', 'wb') as destination:
        for chunk in file_1.chunks():
            destination.write(chunk)

    with open(settings.MEDIA_XMLS_FILES + '/cabecera.xlsx', 'wb') as destination:
        for chunk in file_2.chunks():
            destination.write(chunk)
    data = pd.read_excel(settings.MEDIA_XMLS_FILES + '/cabecera.xlsx')
    head_file = pd.DataFrame(data, columns= [
        'Orden',
        'Número material',
        'Texto breve material',
        'Cantidad orden (GMEIN)',
        'Cantidad entregada (GMEIN)'
    ])

    data = pd.read_excel(settings.MEDIA_XMLS_FILES + '/operaciones.xlsx')
    operation_file = pd.DataFrame(data, columns= [
        'Puesto de trabajo',
        'Txt.brv.operación',
        'Valor pref.2 (VGE02)',
        'Status sistema'
    ])
    try:

        for index, row in head_file.iterrows():
            name_production_line = validate_duplicity(operation_file.iloc[index]['Txt.brv.operación'])
            production_line = ProductionLine.objects.filter(name=name_production_line)
            dxn = DXN.objects.filter(name=row['Número material'])
            production_order = ProductionOrder.objects.filter(order_number=row['Orden'])
            update = False
            production_order_status = None
            if production_order:
                update = True
                production_order_status = production_order.first().production_order_status.name

            if not production_line:
                print("production line not found", operation_file.iloc[index]['Txt.brv.operación'])
                continue

            if not dxn:
                dxn = DXN(
                    name=row['Número material'],
                    description=row['Texto breve material']
                )
                dxn.save()
            else:
                dxn = dxn.first()

            data = {
                'order_number': row['Orden'],
                'area': get_area_name(operation_file.iloc[index]['Puesto de trabajo']),
                'dxn': dxn.id,
                'production_line': production_line.first().id,
                'production_order_status': get_status(int(row['Cantidad orden (GMEIN)']),int(row['Cantidad entregada (GMEIN)'])),
                'scrap': 0,
                'observations': 'Observation',
                'description':row['Texto breve material'],
                'cycle': operation_file.iloc[index]['Valor pref.2 (VGE02)'],
                'schedule_amount_order': int(row['Cantidad orden (GMEIN)']),
                'delivered_amount_order_sap': int(row['Cantidad entregada (GMEIN)']),
                'delivered_amount_order_dpt': int(row['Cantidad entregada (GMEIN)'])
            }
            if update:
                if production_order_status != settings.PRODUCTION_ORDER_STATE_4:
                    print("Update Production Order " + str(row['Orden']))
                    production_serializer = ProductionOrderSerializer(production_order.first(), data=data)
                    if production_serializer.is_valid():
                        production_serializer.save()
                    else:
                        print(production_serializer.errors)
            else:
                print("Create Production Order " + str(row['Orden']))
                production_serializer = ProductionOrderSerializer(data=data)
                if production_serializer.is_valid():
                    production_serializer.save()
                else:
                    print(production_serializer.errors)

        return True

    except Exception as e:
      print("An exception occurred", str(e))
      return False


@permission_classes((IsAuthenticated, ))
def upload_file(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)

        if form.is_valid():
            filename_one = request.FILES['file'].name
            filename_two = request.FILES['file_two'].name

            if filename_one.endswith(settings.EXCEL_VALID_EXTENSION) and \
                filename_two.endswith(settings.EXCEL_VALID_EXTENSION):

                try:
                    process = handle_uploaded_file(
                        request.FILES['file'],
                        request.FILES['file_two']
                    )

                except:
                  process = False

                if not process:
                    return render(
                        request,
                        'error_screen.html', 
                            {
                                'error_message': get_message_by_key('ERROR_MESSAGE'),
                                'message': get_message_by_key('ERROR_PROCESSING_EXCEL_FILE_MESSAGE')
                            }
                    )

            else:
                return render(
                    request,
                    'error_screen.html',
                        {
                            'error_message': get_message_by_key('ERROR_MESSAGE'),
                            'message': get_message_by_key('ERROR_PROCESSING_EXCEL_FILE_MESSAGE')
                        }
                )

            return render(
                request,
                'sucess_screen.html', 
                    {
                        'ok_message': get_message_by_key('OK_MESSAGE'),
                        'message': get_message_by_key('SUCESS_PROCESSING_EXCEL_FILE_MESSAGE')
                    }
            )

        return render(
            request,
            'error_screen.html',
                {
                    'error_message': get_message_by_key('ERROR_MESSAGE'),
                    'message': get_message_by_key('ERROR_PROCESSING_EXCEL_FILE_MESSAGE')
                }
        )
    else:
        form = UploadFileForm()

    return render(
        request,
        'upload.html', 
            {
                'form': form,
                'message_upload_files': get_message_by_key('UPLOAD_FILES_MESSAGE_OPERATION_HEAD'),
                'operations': get_message_by_key('OPERATION'),
                'head': get_message_by_key('HEAD'),
                'message_loading': get_message_by_key('UPLOAD_FILES_MESSAGE_LOADING'),
                'submit_label': get_message_by_key('UPLOAD_SUBMIT_MESSAGE')
            }
    )

