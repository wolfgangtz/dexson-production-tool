from rest_framework import generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from production.models.production_order_status import ProductionOrderStatus
from production.serializers.production_order_status import ProductionOrderStatusSerializer

@permission_classes((IsAuthenticated, ))
class ProductionOrderStatusList(generics.ListCreateAPIView):
    queryset = ProductionOrderStatus.objects.all()
    serializer_class = ProductionOrderStatusSerializer
    name = 'production-order-status-list'

@permission_classes((IsAuthenticated, ))
class ProductionOrderStatusDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ProductionOrderStatus.objects.all()
    serializer_class = ProductionOrderStatusSerializer
    name = 'production-order-status-detail'

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)