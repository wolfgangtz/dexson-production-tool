from rest_framework import generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from production.models.production_order import ProductionOrder
from production.serializers.production_order import ProductionOrderSerializer
from django.db.models import F
from django.shortcuts import render
from production.models.production_line import ProductionLine
from production.helpers.helpers_function import get_message_by_key


RENDER_PRODUCTION_ORDERS_NAME = 'render-production-order'

@permission_classes((IsAuthenticated, ))
class ProductionOrderList(generics.ListCreateAPIView):
    queryset = ProductionOrder.objects.all()
    serializer_class = ProductionOrderSerializer
    name = 'production-order-list'

@permission_classes((IsAuthenticated, ))
class ProductionOrderDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ProductionOrder.objects.all()
    serializer_class = ProductionOrderSerializer
    name = 'production-order-detail'

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

@permission_classes((IsAuthenticated, ))
def render_production_orders(request, production_line_id):
    production_orders = ProductionOrder.objects.filter(production_line__id=production_line_id).exclude(schedule_amount_order=F('delivered_amount_order_sap'))
    return render(
        request,
        'list_op.html',
            {
                'production_orders': production_orders,
                'message_list_production_orders': get_message_by_key('MESSAGE_LIST_PRODUCTION_ORDERS'),
                'production_orders_name': get_message_by_key('MESSAGE_TITTLE_NAME_PRODUCTION_ORDERS'),
                'dxn_name': get_message_by_key('MESSAGE_TITTLE_NAME_DXN'),
                'description_name': get_message_by_key('MESSAGE_TITTLE_NAME_DESCRIPTION'),
                'schedule_amount': get_message_by_key('MESSAGE_TITTLE_NAME_SCHEDULE_AMOUNT'),
                'pending_amount': get_message_by_key('MESSAGE_TITTLE_NAME_PENDING_AMOUNT'),
                'delivered_amount_sap': get_message_by_key('MESSAGE_TITTLE_NAME_DELIVERED_AMOUNT_SAP'),
                'delivered_amount_dpt': get_message_by_key('MESSAGE_TITTLE_NAME_DELIVERED_AMOUNT_DPT'),
                'cicle': get_message_by_key('MESSAGE_TITTLE_NAME_CYCLE'),
                'time_queue': get_message_by_key('MESSAGE_TITTLE_NAME_TIME_QUEUE'),
                'final_date': get_message_by_key('MESSAGE_TITTLE_FINAL_DATE'),
                'production_line_name': ProductionLine.objects.get(id=production_line_id).name
            }
    )