from rest_framework import generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from production.models.production_order_report import ProductionOrderReport
from production.serializers.production_order_report import ProductionOrderReportSerializer

@permission_classes((IsAuthenticated, ))
class ProductionOrderReportList(generics.ListCreateAPIView):
    queryset = ProductionOrderReport.objects.all()
    serializer_class = ProductionOrderReportSerializer
    name = 'production-order-report-list'

@permission_classes((IsAuthenticated, ))
class ProductionOrderReportDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ProductionOrderReport.objects.all()
    serializer_class = ProductionOrderReportSerializer
    name = 'production-order-report-detail'

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)