from django.db import models
from product.models.dxn import DXN
from production.models.area import Area
from production.models.production_line import ProductionLine
from production.models.production_order_status import ProductionOrderStatus


class ProductionOrder(models.Model):

    order_number = models.CharField(
        max_length=50,
        blank=False,
        null=False
    )

    area = models.ForeignKey(
        Area,
        on_delete=models.PROTECT,
        null=False,
        blank=False
    )

    dxn = models.ForeignKey(
        DXN,
        on_delete=models.PROTECT,
        null=False,
        blank=False
    )

    production_line = models.ForeignKey(
        ProductionLine,
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        related_name='production_orders'
    )

    production_order_status = models.ForeignKey(
        ProductionOrderStatus,
        on_delete=models.PROTECT,
        null=False,
        blank=False
    )

    #to store numbers up to 999 with a resolution of 2 decimal places
    scrap = models.DecimalField(
        max_digits=5,
        decimal_places=2
    )

    observations = models.CharField(
        max_length=500,
        blank=True,
        null=True
    )

    #to store numbers up to 999 with a resolution of 3 decimal places
    cycle = models.DecimalField(
        max_digits=6,
        decimal_places=3
    )

    schedule_amount_order = models.PositiveIntegerField(
        default=0
    )

    delivered_amount_order_sap = models.PositiveIntegerField(
        default=0
    )

    delivered_amount_order_dpt = models.PositiveIntegerField(
        default=0
    )

    created_at = models.DateField(
        auto_now_add=True
    )

    updated_at = models.DateField(
        auto_now_add=True
    )

    @property
    def pending_amount(self):
        return self.schedule_amount_order - self.delivered_amount_order_dpt

    @property
    def get_cycle_in_seconds(self):
        cycle = self.cycle
        return round(cycle * 60, 2)
    

    @property
    def get_time_queue(self):
        cycle = self.cycle
        lack_units = self.schedule_amount_order - self.delivered_amount_order_sap
        time_in_minutes = cycle*lack_units
        return round(time_in_minutes/60,2)