from django.db import models


class ProductionOrderStatus(models.Model):

    name = models.CharField(
        max_length=50,
        blank=False,
        null=False
    )

    created_at = models.DateField(
        auto_now_add=True
    )

    updated_at = models.DateField(
        auto_now_add=True
    )