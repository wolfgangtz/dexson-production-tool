from django.db import models
from production.models.area import Area


class ProductionLine(models.Model):

    name = models.CharField(
        max_length=30,
        blank=False,
        null=False
    )

    area = models.ForeignKey(
        Area,
        on_delete=models.PROTECT,
        null=False,
        blank=False
    )

    created_at = models.DateField(
        auto_now_add=True
    )

    updated_at = models.DateField(
        auto_now_add=True
    )

    def __str__(self):
        return self.name

    @property
    def get_lack_time(self):
        counter = 0
        for production_order in self.production_orders.all():
            if production_order.schedule_amount_order != production_order.delivered_amount_order_sap:
                counter = counter + production_order.get_time_queue
        return round(counter, 2)


    @property
    def get_op_queue(self):
        counter = 0
        for production_order in self.production_orders.all():
            if production_order.schedule_amount_order != production_order.delivered_amount_order_sap:
                counter = counter + 1
        return counter