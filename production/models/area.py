from django.db import models


class Area(models.Model):

    name = models.CharField(
        max_length=30,
        blank=False,
        null=False
    )

    created_at = models.DateField(
        auto_now_add=True
    )

    updated_at = models.DateField(
        auto_now_add=True
    )

    def __str__(self):
        return self.name
