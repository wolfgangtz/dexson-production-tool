from django.db import models
from django.contrib.auth import get_user_model
from production.models.production_order import ProductionOrder

class ProductionOrderReport(models.Model):

    User = get_user_model()

    conforming_amount = models.PositiveIntegerField(
        default=0
    )

    #to store numbers up to 999 with a resolution of 2 decimal places
    scrap = models.DecimalField(
        max_digits=5,
        decimal_places=2
    )

    #to store numbers up to 999 with a resolution of 2 decimal places
    real_weight = models.DecimalField(
        max_digits=5,
        decimal_places=2,
        null=True,
        blank=True
    )

    production_order = models.ForeignKey(
        ProductionOrder,
        on_delete=models.PROTECT,
        null=False,
        blank=False
    )

    user = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        null=False,
        blank=False
    )

    created_at = models.DateField(
        auto_now_add=True
    )

    updated_at = models.DateField(
        auto_now_add=True
    )