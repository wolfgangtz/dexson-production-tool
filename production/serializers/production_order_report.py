from rest_framework import serializers
from production.models.production_order_report import ProductionOrderReport


class ProductionOrderReportSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductionOrderReport
        fields = (
            'id',
            'conforming_amount',
            'scrap',
            'real_weight',
            'production_order',
            'user',
            'created_at',
            'updated_at'
        )
        read_only_fields = (
            'id',
        )