from rest_framework import serializers
from production.models.area import Area


class AreaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Area
        fields = (
            'id',
            'name',
            'created_at',
            'updated_at'
        )
        read_only_fields = (
            'id',
        )