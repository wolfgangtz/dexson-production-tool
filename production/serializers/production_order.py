from rest_framework import serializers
from production.models.production_order import ProductionOrder


class ProductionOrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductionOrder
        fields = (
            'id',
            'order_number',
            'area',
            'dxn',
            'production_line',
            'production_order_status',
            'scrap',
            'observations',
            'cycle',
            'schedule_amount_order',
            'delivered_amount_order_sap',
            'delivered_amount_order_dpt',
            'created_at',
            'updated_at'
        )
        read_only_fields = (
            'id',
        )