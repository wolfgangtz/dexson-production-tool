from rest_framework import serializers
from production.models.production_order_status import ProductionOrderStatus


class ProductionOrderStatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductionOrderStatus
        fields = (
            'id',
            'name',
            'created_at',
            'updated_at'
        )
        read_only_fields = (
            'id',
        )