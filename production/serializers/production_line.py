from rest_framework import serializers
from production.models.production_line import ProductionLine


class ProductionLineSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductionLine
        fields = (
            'id',
            'name',
            'area',
            'created_at',
            'updated_at'
        )
        read_only_fields = (
            'id',
        )