from django.urls import path
from django.urls import re_path
from production.views import area
from production.views import production_line
from production.views import production_order
from production.views import production_order_report
from production.views import production_order_status
from production.views import files_management

urlpatterns = [
    re_path(
        '^production-orders/(?P<production_line_id>[0-9]+)/list$',
        production_order.render_production_orders,
        name=production_order.RENDER_PRODUCTION_ORDERS_NAME
    ),
    # upload File
    path(
        'upload-files/',
        files_management.upload_file,
        name='upload-files'
    ),
    re_path(
        '^(?P<area_id>[0-9]+)/list$',
        area.render_production_lines_by_area,
        name=area.RENDER_PRODUCTION_LINES_BY_AREA_NAME
    ),
    path(
        'list-area/',
        area.render_areas,
        name=area.RENDER_AREAS_NAME
    ),
    path(
        'area/',
        area.AreaList.as_view(),
        name=area.AreaList.name
    ),
    re_path(
        'area/^(?P<pk>[0-9]+)/$',
        area.AreaDetail.as_view(),
        name=area.AreaDetail.name
    ),
    path(
        'line/',
        production_line.ProductionLineList.as_view(),
        name=production_line.ProductionLineList.name
    ),
    re_path(
        '^line/(?P<pk>[0-9]+)/$',
        production_line.ProductionLineDetail.as_view(),
        name=production_line.ProductionLineDetail.name
    ),
    path(
        'order/',
        production_order.ProductionOrderList.as_view(),
        name=production_order.ProductionOrderList.name
    ),
    re_path(
        '^order/(?P<pk>[0-9]+)/$',
        production_order.ProductionOrderDetail.as_view(),
        name=production_order.ProductionOrderDetail.name
    ),
    path(
        'order-report/',
        production_order_report.ProductionOrderReportList.as_view(),
        name=production_order_report.ProductionOrderReportList.name
    ),
    re_path(
        '^order-report/(?P<pk>[0-9]+)/$',
        production_order_report.ProductionOrderReportDetail.as_view(),
        name=production_order_report.ProductionOrderReportDetail.name
    ),
    path(
        'order-status/',
        production_order_status.ProductionOrderStatusList.as_view(),
        name=production_order_status.ProductionOrderStatusList.name
    ),
    re_path(
        '^order-status/(?P<pk>[0-9]+)/$',
        production_order_status.ProductionOrderStatusDetail.as_view(),
        name=production_order_status.ProductionOrderStatusDetail.name
    )
]