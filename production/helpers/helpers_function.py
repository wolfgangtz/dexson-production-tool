import os
import json
from django.conf import settings

def get_message_by_key(key):

    messages_dict_path = os.path.join(settings.BASE_DIR, 'static') + "/messages/messages.json"
    message_dict = []
    with open(messages_dict_path) as json_file:
        message_dict = json.load(json_file)
    return message_dict[key]