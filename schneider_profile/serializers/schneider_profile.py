from rest_framework import serializers
from schneider_profile.models.schneider_profile import SchneiderProfile


class SchneiderProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = SchneiderProfile
        fields = (
            'id',
            'on_call',
            'area',
            'work_shift',
            'production_line',
            'emergency_call_type',
            'user',
            'schneider_code',
            'identification_number',
            'created_at',
            'updated_at'
        )
        read_only_fields = (
            'id',
        )