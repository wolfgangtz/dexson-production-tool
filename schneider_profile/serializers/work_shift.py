from rest_framework import serializers
from schneider_profile.models.work_shift import WorkShift


class WorkShiftSerializer(serializers.ModelSerializer):

    class Meta:
        model = WorkShift
        fields = (
            'id',
            'shift',
            'created_at',
            'updated_at'
        )
        read_only_fields = (
            'id',
        )