from rest_framework import generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from schneider_profile.models.work_shift import WorkShift
from schneider_profile.serializers.work_shift import WorkShiftSerializer

@permission_classes((IsAuthenticated, ))
class WorkShiftList(generics.ListCreateAPIView):
    queryset = WorkShift.objects.all()
    serializer_class = WorkShiftSerializer
    name = 'work-shift-list'

@permission_classes((IsAuthenticated, ))
class WorkShiftDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = WorkShift.objects.all()
    serializer_class = WorkShiftSerializer
    name = 'work-shift-detail'

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)