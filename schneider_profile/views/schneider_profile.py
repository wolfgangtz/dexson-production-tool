from rest_framework import generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from schneider_profile.models.schneider_profile import SchneiderProfile
from schneider_profile.serializers.schneider_profile import SchneiderProfileSerializer
from schneider_profile.forms import LoginForm
from production.forms import UploadFileForm
from django.shortcuts import render
from production.helpers.helpers_function import get_message_by_key
from rest_framework.authtoken.models import Token
from django.contrib.auth import get_user_model
from django.contrib.auth import login as django_login
from django.contrib.auth import authenticate
from django.conf import settings

LOGIN_NAME = 'user-login'

@permission_classes((IsAuthenticated, ))
class SchneiderProfileList(generics.ListCreateAPIView):
    queryset = SchneiderProfile.objects.all()
    serializer_class = SchneiderProfileSerializer
    name = 'schneider-profile-list'

@permission_classes((IsAuthenticated, ))
class SchneiderProfileDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = SchneiderProfile.objects.all()
    serializer_class = SchneiderProfileSerializer
    name = 'schneider-profile-detail'

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)


def redirect(user, request):
    groups = user.groups.all()
    group_names = [group.name for group in groups]

    if settings.GROUP_NAME_1 in group_names:
        print("Soporte")
    elif settings.GROUP_NAME_2 in group_names:
        print("Operador")
    elif settings.GROUP_NAME_3 in group_names:
        print("Administrador")
        form = UploadFileForm()

        return render(
            request,
            'upload.html', 
                {
                    'form': form,
                    'message_upload_files': get_message_by_key('UPLOAD_FILES_MESSAGE_OPERATION_HEAD'),
                    'operations': get_message_by_key('OPERATION'),
                    'head': get_message_by_key('HEAD'),
                    'message_loading': get_message_by_key('UPLOAD_FILES_MESSAGE_LOADING'),
                    'submit_label': get_message_by_key('UPLOAD_SUBMIT_MESSAGE')
                }
        )
    elif settings.GROUP_NAME_4 in group_names:
        print("Monitor")

def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            identification_number = request.POST['username']
            password = request.POST['password']
            User = get_user_model()
            profile = SchneiderProfile.objects.filter(
                identification_number=identification_number
            )
            if not profile:
                print("Porfile not found")
                return render(
                    request,
                    'error_screen.html',
                        {
                            'error_message': get_message_by_key('ERROR_MESSAGE'),
                            'message': get_message_by_key('ERROR_LOGIN_DESCRIPTION')
                        }
                )

            user = profile.first().user

            login_validation = authenticate(username=user.username, password=password)
            if not login_validation:
                print("Invalid Password")
                return render(
                request,
                'error_screen.html',
                    {
                        'error_message': get_message_by_key('ERROR_MESSAGE'),
                        'message': get_message_by_key('ERROR_LOGIN_DESCRIPTION')
                    }
                )

            token, created = Token.objects.get_or_create(user=user)
            django_login(request, user)
            return redirect(user, request)

            return render(
                request,
                'sucess_screen.html', 
                    {
                        'ok_message': get_message_by_key('OK_MESSAGE'),
                        'message': get_message_by_key('SUCESS_PROCESSING_EXCEL_FILE_MESSAGE')
                    }
            )

        return render(
            request,
            'error_screen.html',
                {
                    'error_message': get_message_by_key('ERROR_MESSAGE'),
                    'message': get_message_by_key('ERROR_LOGIN_DESCRIPTION')
                }
        )

    else:
        if request.user.is_authenticated:
            print("YA HAY USUARIO", request.user)
            return redirect(request.user, request)

        form = LoginForm()

    return render(
        request,
        'login.html',
            {
                'form': form,
                'login': get_message_by_key('LOGIN_MESSAGE'),
                'submit_label_login': get_message_by_key('LOGIN_SUBMIT_MESSAGE'),
                'username': get_message_by_key('LOGIN_USERNAME_MESSAGE'),
                'password': get_message_by_key('LOGIN_PASSWORD_MESSAGE')
            }
    )