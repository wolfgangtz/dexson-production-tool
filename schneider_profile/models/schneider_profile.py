from django.db import models
from django.contrib.auth import get_user_model
from production.models.area import Area
from production.models.production_line import ProductionLine
from schneider_profile.models.work_shift import WorkShift
from emergency_call.models.emergency_call_type import EmergencyCallType


class SchneiderProfile(models.Model):

    User = get_user_model()

    on_call = models.BooleanField(
        default=False
    )

    area = models.ForeignKey(
        Area,
        on_delete=models.PROTECT,
        null=False,
        blank=False
    )

    work_shift = models.ForeignKey(
        WorkShift,
        on_delete=models.PROTECT,
        null=False,
        blank=False
    )

    production_line = models.ForeignKey(
        ProductionLine,
        on_delete=models.PROTECT,
        null=False,
        blank=False
    )

    emergency_call_type = models.ForeignKey(
        EmergencyCallType,
        on_delete=models.PROTECT,
        null=False,
        blank=False
    )

    user = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        null=False,
        blank=False
    )

    schneider_code = models.CharField(
        max_length=30,
        blank=False,
        null=False
    )

    identification_number = models.CharField(
        max_length=30,
        blank=False,
        null=False
    )

    created_at = models.DateField(
        auto_now_add=True
    )

    updated_at = models.DateField(
        auto_now_add=True
    )