from django import forms

class LoginForm(forms.Form):
    username = forms.CharField(label='Nombre de Usuario', max_length=50)
    password = forms.CharField(label='Contraseña', max_length=50, widget=forms.PasswordInput)