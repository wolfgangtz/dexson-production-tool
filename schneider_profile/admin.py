from django.contrib import admin
from schneider_profile.models.schneider_profile import SchneiderProfile

class SchneiderProfileAdmin(admin.ModelAdmin):
    list_display = (
        'on_call',
        'area',
        'work_shift',
        'production_line',
        'emergency_call_type',
        'user',
        'schneider_code',
        'identification_number'
    )

admin.site.register(SchneiderProfile, SchneiderProfileAdmin)