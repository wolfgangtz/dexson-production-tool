from django.urls import path
from django.urls import re_path
from schneider_profile.views import schneider_profile
from schneider_profile.views import work_shift

urlpatterns = [
    path(
        'login/',
        schneider_profile.login,
        name=schneider_profile.LOGIN_NAME
    ),
    path(
        '',
        schneider_profile.SchneiderProfileList.as_view(),
        name=schneider_profile.SchneiderProfileList.name
    ),
    re_path(
        '^(?P<pk>[0-9]+)/$',
        schneider_profile.SchneiderProfileDetail.as_view(),
        name=schneider_profile.SchneiderProfileDetail.name
    ),
    path(
        'work-shift/',
        work_shift.WorkShiftList.as_view(),
        name=work_shift.WorkShiftList.name
    ),
    re_path(
        '^work-shift/(?P<pk>[0-9]+)/$',
        work_shift.WorkShiftDetail.as_view(),
        name=work_shift.WorkShiftDetail.name
    )
]