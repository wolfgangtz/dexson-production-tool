from django.apps import AppConfig


class SchneiderProfileConfig(AppConfig):
    name = 'schneider_profile'
