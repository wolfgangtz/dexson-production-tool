# Generated by Django 2.2.4 on 2019-08-11 04:38
import json
from schneider_profile.serializers.work_shift import WorkShiftSerializer
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
from django.contrib.auth.models import Group, Permission 
from django.contrib.contenttypes.models import ContentType 
from django.contrib.auth import get_user_model

def initialize_work_shifts(apps, schema_editor):
    print('initializing Work Shifts')
    with open(settings.BASE_DIR + '/schneider_profile/initial_data/work_shift.json', 'r') as work_shift:
        work_shift_serializer = WorkShiftSerializer(data=json.load(work_shift), many=True)
        if work_shift_serializer.is_valid():
            work_shift_serializer.save()

def create_default_admin_user(apps, schema_editor):
    print('creating default admin user')
    User = get_user_model()
    admin_user = User.objects.create_superuser('admin','admin@dexson.com', '<MYr"&kF%t6;,(9C')
    admin_user.save()

def create_groups(apps, schema_editor):
    print('creating groups')
    User = get_user_model()
    support_group, created = Group.objects.get_or_create(name ='Soporte') 
    #ct = ContentType.objects.get_for_model(User) 
    #permission = Permission.objects.create(
    #    codename ='can_go_haridwar',
    #    name ='Can go to Haridwar',
    #    content_type = ct
    #) 
    #support_group.permissions.add(permission)

    operator_group, created = Group.objects.get_or_create(name ='Operador') 
    #permission = Permission.objects.create(
    #    codename ='can_go_haridwar',
    #    name ='Can go to Haridwar',
    #    content_type = ct
    #) 
    #operator_group.permissions.add(permission)

    admin_group, created = Group.objects.get_or_create(name ='Administrador') 
    #permission = Permission.objects.create(
    #    codename ='can_go_haridwar',
    #    name ='Can go to Haridwar',
    #    content_type = ct
    #) 
    #admin_group.permissions.add(permission)

    monitor_group, created = Group.objects.get_or_create(name ='Monitor')
    #permission = Permission.objects.create(
    #    codename ='can_go_haridwar',
    #    name ='Can go to Haridwar',
    #    content_type = ct
    #) 
    #monitor_group.permissions.add(permission)


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('emergency_call', '0001_initial'),
        ('production', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='WorkShift',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('shift', models.CharField(max_length=30)),
                ('created_at', models.DateField(auto_now_add=True)),
                ('updated_at', models.DateField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='SchneiderProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('on_call', models.BooleanField(default=False)),
                ('schneider_code', models.CharField(max_length=30)),
                ('identification_number', models.CharField(max_length=30)),
                ('created_at', models.DateField(auto_now_add=True)),
                ('updated_at', models.DateField(auto_now_add=True)),
                ('area', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='production.Area')),
                ('emergency_call_type', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='emergency_call.EmergencyCallType')),
                ('production_line', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='production.ProductionLine')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
                ('work_shift', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='schneider_profile.WorkShift')),
            ],
        ),
        migrations.RunPython(initialize_work_shifts),
        migrations.RunPython(create_default_admin_user),
        migrations.RunPython(create_groups),
    ]
