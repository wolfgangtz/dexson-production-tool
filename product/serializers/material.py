from rest_framework import serializers
from product.models.material import Material


class MaterialSerializer(serializers.ModelSerializer):

    class Meta:
        model = Material
        fields = (
            'id',
            'name',
            'description',
            'cost',
            'currency',
            'created_at',
            'updated_at'
        )
        read_only_fields = (
            'id',
        )