from rest_framework import serializers
from product.models.dxn import DXN


class DXNSerializer(serializers.ModelSerializer):

    class Meta:
        model = DXN
        fields = (
            'id',
            'name',
            'description',
            'created_at',
            'updated_at'
        )
        read_only_fields = (
            'id',
        )