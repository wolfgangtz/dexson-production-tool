from rest_framework import serializers
from product.models.material_dxn import MaterialDXN


class MaterialDXNSerializer(serializers.ModelSerializer):

    class Meta:
        model = MaterialDXN
        fields = (
            'id',
            'material',
            'dxn',
            'quantity',
            'measure',
            'created_at',
            'updated_at'
        )
        read_only_fields = (
            'id',
        )