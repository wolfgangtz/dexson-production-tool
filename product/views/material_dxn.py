from rest_framework import generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from product.models.material_dxn import MaterialDXN
from product.serializers.material_dxn import MaterialDXNSerializer

@permission_classes((IsAuthenticated, ))
class MaterialDXNList(generics.ListCreateAPIView):
    queryset = MaterialDXN.objects.all()
    serializer_class = MaterialDXNSerializer
    name = 'material-dxn-list'

@permission_classes((IsAuthenticated, ))
class MaterialDXNDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = MaterialDXN.objects.all()
    serializer_class = MaterialDXNSerializer
    name = 'material-dxn-detail'

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)