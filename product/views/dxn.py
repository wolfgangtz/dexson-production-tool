from rest_framework import generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from product.models.dxn import DXN
from product.serializers.dxn import DXNSerializer

@permission_classes((IsAuthenticated, ))
class DXNList(generics.ListCreateAPIView):
    queryset = DXN.objects.all()
    serializer_class = DXNSerializer
    name = 'dxn-list'

@permission_classes((IsAuthenticated, ))
class DXNDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = DXN.objects.all()
    serializer_class = DXNSerializer
    name = 'dxn-detail'

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)