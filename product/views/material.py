from rest_framework import generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from product.models.material import Material
from product.serializers.material import MaterialSerializer

@permission_classes((IsAuthenticated, ))
class MaterialList(generics.ListCreateAPIView):
    queryset = Material.objects.all()
    serializer_class = MaterialSerializer
    name = 'material-list'

@permission_classes((IsAuthenticated, ))
class MaterialDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Material.objects.all()
    serializer_class = MaterialSerializer
    name = 'material-detail'

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)