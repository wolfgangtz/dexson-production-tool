from django.db import models


class Material(models.Model):

    name = models.CharField(
        max_length=50,
        blank=False,
        null=False
    )

    description = models.CharField(
        max_length=150,
        blank=False,
        null=False
    )

    #to store numbers up to 999.999 with a resolution of 2 decimal places
    cost = models.DecimalField(
        max_digits=8,
        decimal_places=2
    )

    currency = models.CharField(
        max_length=5,
        blank=False,
        null=False
    )

    created_at = models.DateField(
        auto_now_add=True
    )

    updated_at = models.DateField(
        auto_now_add=True
    )