from django.db import models
from product.models.dxn import DXN
from product.models.material import Material

class MaterialDXN(models.Model):


    material = models.ForeignKey(
        Material,
        on_delete=models.PROTECT,
        null=False,
        blank=False
    )

    dxn = models.ForeignKey(
        DXN,
        on_delete=models.PROTECT,
        null=False,
        blank=False
    )

    #to store numbers up to 999 with a resolution of 2 decimal places
    quantity = models.DecimalField(
        max_digits=5,
        decimal_places=2
    )

    measure = models.CharField(
        max_length=5,
        blank=False,
        null=False
    )

    created_at = models.DateField(
        auto_now_add=True
    )

    updated_at = models.DateField(
        auto_now_add=True
    )