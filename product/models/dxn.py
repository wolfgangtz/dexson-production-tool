from django.db import models


class DXN(models.Model):

    name = models.CharField(
        max_length=30,
        blank=False,
        null=False
    )

    description = models.CharField(
        max_length=150,
        blank=False,
        null=False
    )

    created_at = models.DateField(
        auto_now_add=True
    )

    updated_at = models.DateField(
        auto_now_add=True
    )