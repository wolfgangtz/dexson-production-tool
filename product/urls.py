from django.urls import path
from django.urls import re_path
from product.views import dxn
from product.views import material
from product.views import material_dxn

urlpatterns = [
    path(
        'dxn/',
        dxn.DXNList.as_view(),
        name=dxn.DXNList.name
    ),
    re_path(
        'dxn/^(?P<pk>[0-9]+)/$',
        dxn.DXNDetail.as_view(),
        name=dxn.DXNDetail.name
    ),
    path(
        'material/',
        material.MaterialList.as_view(),
        name=material.MaterialList.name
    ),
    re_path(
        '^material/(?P<pk>[0-9]+)/$',
        material.MaterialDetail.as_view(),
        name=material.MaterialDetail.name
    ),
    path(
        'material-dxn/',
        material_dxn.MaterialDXNList.as_view(),
        name=material_dxn.MaterialDXNList.name
    ),
    re_path(
        '^material-dxn/(?P<pk>[0-9]+)/$',
        material_dxn.MaterialDXNDetail.as_view(),
        name=material_dxn.MaterialDXNDetail.name
    )
]