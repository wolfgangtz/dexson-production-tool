from django.apps import AppConfig


class NonConformingConfig(AppConfig):
    name = 'non_conforming'
