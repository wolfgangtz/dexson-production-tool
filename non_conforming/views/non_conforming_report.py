from rest_framework import generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from non_conforming.models.non_conforming_report import NonConformingReport
from non_conforming.serializers.non_conforming_report import NonConformingReportSerializer

@permission_classes((IsAuthenticated, ))
class NonConformingReportList(generics.ListCreateAPIView):
    queryset = NonConformingReport.objects.all()
    serializer_class = NonConformingReportSerializer
    name = 'non-conforming-report-list'

@permission_classes((IsAuthenticated, ))
class NonConformingReportDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = NonConformingReport.objects.all()
    serializer_class = NonConformingReportSerializer
    name = 'non-conforming-report-detail'

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)