from rest_framework import generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from non_conforming.models.non_conforming_type import NonConformingType
from non_conforming.serializers.non_conforming_type import NonConformingTypeSerializer

@permission_classes((IsAuthenticated, ))
class NonConformingTypeList(generics.ListCreateAPIView):
    queryset = NonConformingType.objects.all()
    serializer_class = NonConformingTypeSerializer
    name = 'non-conforming-type-list'

@permission_classes((IsAuthenticated, ))
class NonConformingTypeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = NonConformingType.objects.all()
    serializer_class = NonConformingTypeSerializer
    name = 'non-conforming-type-detail'

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)