from rest_framework import generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from non_conforming.models.non_conforming_category import NonConformingCategory
from non_conforming.serializers.non_conforming_category import NonConformingCategorySerializer

@permission_classes((IsAuthenticated, ))
class NonConformingCategoryList(generics.ListCreateAPIView):
    queryset = NonConformingCategory.objects.all()
    serializer_class = NonConformingCategorySerializer
    name = 'non-conforming-category-list'

@permission_classes((IsAuthenticated, ))
class NonConformingCategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = NonConformingCategory.objects.all()
    serializer_class = NonConformingCategorySerializer
    name = 'non-conforming-category-detail'

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)