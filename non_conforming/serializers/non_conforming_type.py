from rest_framework import serializers
from non_conforming.models.non_conforming_type import NonConformingType


class NonConformingTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = NonConformingType
        fields = (
            'id',
            'name',
            'non_conforming_category',
            'created_at',
            'updated_at'
        )
        read_only_fields = (
            'id',
        )