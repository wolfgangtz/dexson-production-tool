from rest_framework import serializers
from non_conforming.models.non_conforming_report import NonConformingReport


class NonConformingReportSerializer(serializers.ModelSerializer):

    class Meta:
        model = NonConformingReport
        fields = (
            'id',
            'value',
            'material',
            'non_conforming_type',
            'production_order_report',
            'non_conforming_category',
            'observations',
            'created_at',
            'updated_at'
        )
        read_only_fields = (
            'id',
        )