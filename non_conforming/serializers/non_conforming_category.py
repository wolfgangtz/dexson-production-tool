from rest_framework import serializers
from non_conforming.models.non_conforming_category import NonConformingCategory


class NonConformingCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = NonConformingCategory
        fields = (
            'id',
            'name',
            'request_value',
            'request_non_confirming_type',
            'request_observations',
            'request_material',
            'created_at',
            'updated_at'
        )
        read_only_fields = (
            'id',
        )