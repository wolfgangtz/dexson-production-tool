from django.urls import path
from django.urls import re_path
from non_conforming.views import non_conforming_category
from non_conforming.views import non_conforming_report
from non_conforming.views import non_conforming_type

urlpatterns = [
    path(
        'category/',
        non_conforming_category.NonConformingCategoryList.as_view(),
        name=non_conforming_category.NonConformingCategoryList.name
    ),
    re_path(
        'category/^(?P<pk>[0-9]+)/$',
        non_conforming_category.NonConformingCategoryDetail.as_view(),
        name=non_conforming_category.NonConformingCategoryDetail.name
    ),
    path(
        'report/',
        non_conforming_report.NonConformingReportList.as_view(),
        name=non_conforming_report.NonConformingReportList.name
    ),
    re_path(
        '^report/(?P<pk>[0-9]+)/$',
        non_conforming_report.NonConformingReportDetail.as_view(),
        name=non_conforming_report.NonConformingReportDetail.name
    ),
    path(
        'type/',
        non_conforming_type.NonConformingTypeList.as_view(),
        name=non_conforming_type.NonConformingTypeList.name
    ),
    re_path(
        '^type/(?P<pk>[0-9]+)/$',
        non_conforming_type.NonConformingTypeDetail.as_view(),
        name=non_conforming_type.NonConformingTypeDetail.name
    )
]