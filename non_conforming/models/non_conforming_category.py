from django.db import models

class NonConformingCategory(models.Model):

    name = models.CharField(
        max_length=50,
        blank=False,
        null=False
    )

    request_value = models.BooleanField(
        default=False
    )

    request_non_confirming_type = models.BooleanField(
        default=False
    )

    request_observations = models.BooleanField(
        default=False
    )

    request_material = models.BooleanField(
        default=False
    )

    created_at = models.DateField(
        auto_now_add=True
    )

    updated_at = models.DateField(
        auto_now_add=True
    )