from django.db import models
from product.models.material import Material
from production.models.production_order_report import ProductionOrderReport
from non_conforming.models.non_conforming_category import NonConformingCategory
from non_conforming.models.non_conforming_type import NonConformingType

class NonConformingReport(models.Model):

    #to store numbers up to 999 with a resolution of 2 decimal places
    value = models.DecimalField(
        max_digits=5,
        decimal_places=2
    )

    material = models.ForeignKey(
        Material,
        on_delete=models.PROTECT,
        null=True,
        blank=True
    )

    non_conforming_type = models.ForeignKey(
        NonConformingType,
        on_delete=models.PROTECT,
        null=True,
        blank=True
    )

    production_order_report = models.ForeignKey(
        ProductionOrderReport,
        on_delete=models.PROTECT,
        null=False,
        blank=False
    )

    non_conforming_category = models.ForeignKey(
        NonConformingCategory,
        on_delete=models.PROTECT,
        null=False,
        blank=False
    )

    observations = models.CharField(
        max_length=200,
        blank=True,
        null=True
    )

    created_at = models.DateField(
        auto_now_add=True
    )

    updated_at = models.DateField(
        auto_now_add=True
    )