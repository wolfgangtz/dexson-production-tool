from django.db import models
from non_conforming.models.non_conforming_category import NonConformingCategory


class NonConformingType(models.Model):

    name = models.CharField(
        max_length=50,
        blank=False,
        null=False
    )

    non_conforming_category = models.ForeignKey(
        NonConformingCategory,
        on_delete=models.PROTECT,
        null=False,
        blank=False
    )

    created_at = models.DateField(
        auto_now_add=True
    )

    updated_at = models.DateField(
        auto_now_add=True
    )