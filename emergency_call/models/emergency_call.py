from django.db import models
from django.contrib.auth import get_user_model
from emergency_call.models.emergency_call_type import EmergencyCallType

class EmergencyCall(models.Model):

    User = get_user_model()

    requester_user = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        related_name='requester_user'
    )


    attender_user = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        null=False,
        blank=False,
        related_name='attender_user'
    )

    emergency_call_type = models.ForeignKey(
        EmergencyCallType,
        on_delete=models.PROTECT,
        null=False,
        blank=False
    )

    observations = models.CharField(
        max_length=150,
        null=True,
        blank=True
    )

    created_at = models.DateField(
        auto_now_add=True
    )

    updated_at = models.DateField(
        auto_now_add=True
    )