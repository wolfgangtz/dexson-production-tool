from django.apps import AppConfig


class EmergencyCallConfig(AppConfig):
    name = 'emergency_call'
