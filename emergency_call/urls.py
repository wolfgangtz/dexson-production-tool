from django.urls import path
from django.urls import re_path
from emergency_call.views import emergency_call
from emergency_call.views import emergency_call_type

urlpatterns = [
    path(
        '',
        emergency_call.EmergencyCallList.as_view(),
        name=emergency_call.EmergencyCallList.name
    ),
    re_path(
        '^(?P<pk>[0-9]+)/$',
        emergency_call.EmergencyCallDetail.as_view(),
        name=emergency_call.EmergencyCallDetail.name
    ),
    path(
        'type/',
        emergency_call_type.EmergencyCallTypeList.as_view(),
        name=emergency_call_type.EmergencyCallTypeList.name
    ),
    re_path(
        '^type/(?P<pk>[0-9]+)/$',
        emergency_call_type.EmergencyCallTypeDetail.as_view(),
        name=emergency_call_type.EmergencyCallTypeDetail.name
    )
]