# Generated by Django 2.2.4 on 2019-08-11 00:27
import json
from emergency_call.serializers.emergency_call_type import EmergencyCallTypeSerializer
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion

def initialize_emergency_call_types(apps, schema_editor):
    print('initializing Emergency Call Types')
    with open(settings.BASE_DIR + '/emergency_call/initial_data/emergency_call_type.json', 'r') as emergency_call_type:
        emergency_call_type_serializer = EmergencyCallTypeSerializer(data=json.load(emergency_call_type), many=True)
        if emergency_call_type_serializer.is_valid():
            emergency_call_type_serializer.save()

class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='EmergencyCallType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('created_at', models.DateField(auto_now_add=True)),
                ('updated_at', models.DateField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='EmergencyCall',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('observations', models.CharField(blank=True, max_length=150, null=True)),
                ('created_at', models.DateField(auto_now_add=True)),
                ('updated_at', models.DateField(auto_now_add=True)),
                ('attender_user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='attender_user', to=settings.AUTH_USER_MODEL)),
                ('emergency_call_type', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='emergency_call.EmergencyCallType')),
                ('requester_user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='requester_user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RunPython(initialize_emergency_call_types),
    ]
