from rest_framework import generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from emergency_call.models.emergency_call import EmergencyCall
from emergency_call.serializers.emergency_call import EmergencyCallSerializer

@permission_classes((IsAuthenticated, ))
class EmergencyCallList(generics.ListCreateAPIView):
    queryset = EmergencyCall.objects.all()
    serializer_class = EmergencyCallSerializer
    name = 'emergency-call-list'

@permission_classes((IsAuthenticated, ))
class EmergencyCallDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = EmergencyCall.objects.all()
    serializer_class = EmergencyCallSerializer
    name = 'emergency-call-detail'

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)