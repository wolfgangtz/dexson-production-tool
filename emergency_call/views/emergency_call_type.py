from rest_framework import generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from emergency_call.models.emergency_call_type import EmergencyCallType
from emergency_call.serializers.emergency_call_type import EmergencyCallTypeSerializer

@permission_classes((IsAuthenticated, ))
class EmergencyCallTypeList(generics.ListCreateAPIView):
    queryset = EmergencyCallType.objects.all()
    serializer_class = EmergencyCallTypeSerializer
    name = 'emergency-call-type-list'

@permission_classes((IsAuthenticated, ))
class EmergencyCallTypeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = EmergencyCallType.objects.all()
    serializer_class = EmergencyCallTypeSerializer
    name = 'emergency-call-type-detail'

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)