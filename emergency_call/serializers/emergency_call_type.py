from rest_framework import serializers
from emergency_call.models.emergency_call_type import EmergencyCallType


class EmergencyCallTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = EmergencyCallType
        fields = (
            'id',
            'name',
            'created_at',
            'updated_at'
        )
        read_only_fields = (
            'id',
        )
