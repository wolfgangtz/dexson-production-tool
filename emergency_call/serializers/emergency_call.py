from rest_framework import serializers
from emergency_call.models.emergency_call import EmergencyCall


class EmergencyCallSerializer(serializers.ModelSerializer):

    class Meta:
        model = EmergencyCall
        fields = (
            'id',
            'requester_user',
            'attender_user',
            'emergency_call_type',
            'observations',
            'created_at',
            'updated_at'
        )
        read_only_fields = (
            'id',
        )