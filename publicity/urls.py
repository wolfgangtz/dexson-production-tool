from django.urls import path
from django.urls import re_path
from publicity.views import publicity

urlpatterns = [
    path(
        '',
        publicity.PublicityList.as_view(),
        name=publicity.PublicityList.name
    ),
    re_path(
        '^(?P<pk>[0-9]+)/$',
        publicity.PublicityDetail.as_view(),
        name=publicity.PublicityDetail.name
    )
]