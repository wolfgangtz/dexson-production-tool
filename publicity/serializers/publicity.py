from rest_framework import serializers
from publicity.models.publicity import Publicity


class PublicitySerializer(serializers.ModelSerializer):

    class Meta:
        model = Publicity
        fields = (
            'id',
            'url',
            'created_at',
            'updated_at'
        )
        read_only_fields = (
            'id',
        )