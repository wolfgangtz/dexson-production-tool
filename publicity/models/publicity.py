from django.db import models


class Publicity(models.Model):

    url = models.CharField(
        max_length=150,
        blank=False,
        null=False
    )

    created_at = models.DateField(
        auto_now_add=True
    )

    updated_at = models.DateField(
        auto_now_add=True
    )