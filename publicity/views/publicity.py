from rest_framework import generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated
from publicity.models.publicity import Publicity
from publicity.serializers.publicity import PublicitySerializer

@permission_classes((IsAuthenticated, ))
class PublicityList(generics.ListCreateAPIView):
    queryset = Publicity.objects.all()
    serializer_class = PublicitySerializer
    name = 'publicity-list'

@permission_classes((IsAuthenticated, ))
class PublicityDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Publicity.objects.all()
    serializer_class = PublicitySerializer
    name = 'publicity-detail'

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)