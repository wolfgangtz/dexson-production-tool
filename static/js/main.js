$( document ).ready(function() {

Date.prototype.addHours = function(h) {
  this.setTime(this.getTime() + (h*60*60*1000));
  return this;
}

Date.prototype.substractHours = function(h) {
  this.setTime(this.getTime() - (h*60*60*1000));
  return this;
}

function appendLeadingZeroes(n){
  if(n <= 9){
    return "0" + n;
  }
  return n
}

const monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
  "Julio", "Agosto", "Septiempre", "Octubre", "Noviembre", "Diciembre"
];

$('#datePicker').change(function(){
	parse_hours(this.value);
});


function parse_hours(date_to_set) {
	var currentDate = new Date(date_to_set);
	//currentDate.substractHours(5)
	var rows = document.getElementById("table").rows;
	var hoursToAdd = 0
	for (i = 1; i < rows.length; i++) {
		row = rows[i]
		hoursToAdd = (hoursToAdd + parseFloat(row.getElementsByTagName("td")[8].innerHTML))
		currentDate.addHours(hoursToAdd)
		var formatted_date = currentDate.getDate() + " de " + (monthNames[currentDate.getMonth()])  + " " + appendLeadingZeroes(currentDate.getHours()) + ":" + appendLeadingZeroes(currentDate.getMinutes())
		row.getElementsByTagName("td")[9].innerHTML = formatted_date
	}
}

currentDateSet = new Date()
//currentDateSet.substractHours(0)
let formatted_date = currentDateSet.getFullYear() + "-" + appendLeadingZeroes(currentDateSet.getMonth() + 1) + "-" + appendLeadingZeroes(currentDateSet.getDate()) + "T" + appendLeadingZeroes(currentDateSet.getHours()) + ":" + appendLeadingZeroes(currentDateSet.getMinutes())
$("#datePicker").val(formatted_date);
parse_hours(formatted_date)

});